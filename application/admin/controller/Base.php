<?php
/**
 * Created by PhpStorm.
 * User: 哦哟
 * Date: 2019.3.12
 * Time: 14:35
 */

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Session;

class Base extends Controller
{

    //查出权限写入session
    public function __construct(Request $request = null)
    {

        //登录成功通过SESSION 里的id查权限
        $row = Db::table('u_r')
            ->alias('ur')
            ->field(['r.name'=>'rname','r.work'=>'rwork','f.name'=>'fname','f.controller'=>'controller','f.function'=>'function'])
            ->join('role r', 'ur.r_id = r.id')
            ->join('r_f rf','r.id=rf.r_id')
            ->join('fun f','rf.f_id=f.id')
            ->where('ur.u_id','=',Session::get('id','think'))
            ->select();

        //拥有的权限
        $fun = [];
        foreach ($row as $k => $v) {
            array_push($fun,$k=array($v['controller']=>$v['function']));
        }

        //存入session
        foreach ($fun as $a => $b){
            foreach ($b as $c => $d){
                Session::set($d,$c,$c);
            }
        }

        //增加默认权限
        Session::set('index','index','index');
        Session::set('logindo','index','index');
        Session::set('logout','index','index');
        //判断权限
//        var_dump($_SESSION);die;

        //当前所在模块
        $con = strtolower($request->controller());
        $func = $request->action();



        if ((Session::has($func,$con)) === false){
            return $this->error('你的权限不够');
        }
//        var_dump($request->module(),$request->controller(),$request->action()); die;
//        $this->error('你的权限不够')
    }

}