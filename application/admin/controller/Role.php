<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Session;

class Role extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {

            $list = Db::table('role')->field(['id','name','work'])->order(['id'=>'ASC'])->select();

            foreach ($list as $k => $v) {
                $fun = Db::field(['f.name'=>'fname','r.r_id'=>'rid'])
                    ->table(['r_f'=>'r','fun'=>'f'])
                    ->where('r.f_id=f.id','r.r_id='.$v['id'])
                    ->select();
            }

            return view('role/index',[
                'list' => $list,
                'fun' => $fun
            ]);


    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        return view('role/create');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->post();

        if ($data['name'] == null){
            return $this->error('姓名不能为空');
        } else if ($data['work'] == null){
            return $this->error('工作内容不能为空');
        }


        $result = Db::table('role')->data($data)->insert();

        if ($result > 0) {
            return $this->success('安排上了~', url('admin/role/index'));
        } else {
            return $this->error('还未安排上,请换个姿势');
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
