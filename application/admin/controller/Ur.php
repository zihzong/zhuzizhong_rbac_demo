<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Session;
use app\admin\controller\Base;

class Ur extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $p = $request->post();

        if (!isset($p['id'])) {
            $p['id'] = [0 => 6];
        }

        //先删除已有的角色
        Db::table('u_r')->where('u_id', $p['uid'])->delete();

        //在插入传过来的新值
        foreach ($p['id'] as $k => $v) {
            $data = [
                'u_id' => $p['uid'],
                'r_id' => $v
            ];
            $result = Db::table('u_r')->data($data)->insert();

        }

        if ($result > 0) {
            return $this->success('修改成功咯~', url('admin/user/index'));
        } else {
            return $this->error('修改失败咯~ 请重试', url('admin/user/index'));
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function read($id)
    {

        $data = Db::table('role')->field(['id', 'name'])->select();
        $list = Db::table('u_r')->field(['r_id'])->where(['u_id' => $id])->select();
        $uname = Db::table('user')->field(['name'])->where(['id' => $id])->find();

        //$list中的r_id存入数组 就是多选框的默认选中
        $array = [];
        foreach ($list as $k => $v) {
            if ($v['r_id'] == null) {
                $array[] = 0;
            }
            $array[] = $v['r_id'];
        }

        return view('ur/read', [
            'id' => $id,
            'data' => $data,
            'arr' => $array,
            'uname' => $uname['name']
        ]);


    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request $request
     * @param  int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
