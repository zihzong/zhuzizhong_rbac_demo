<?php

namespace app\admin\controller;

use think\Controller;

class Main extends Base
{
    /**
     * 后台主页
     * @return \think\response\View
     */
    public function index()
    {
        return view('admin@main/index');
    }

}
