<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;

class Rf extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $p = $request->post();

        //先删除已有的权限
        Db::table('r_f')->where('r_id', $p['uid'])->delete();

        foreach ($p['id'] as $k => $v) {
            $data = [
                'r_id' => $p['uid'],
                'f_id' => $v
            ];
            $result = Db::table('r_f')->data($data)->insert();

        }

        if ($result > 0) {
            return $this->success('修改成功咯~', url('admin/role/index'));
        } else {
            return $this->error('修改失败咯~ 请重试', url('admin/role/index'));
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        $data = Db::table('fun')->field(['id','name','controller','function'])->select();
        $list = Db::table('r_f')->field(['f_id'])->where(['r_id'=>$id])->select();
        $uname = Db::table('role')->field(['name'])->where(['id'=>$id])->find();



        //$list中的f_id存入数组 就是多选框的默认选中
        foreach ($list as $k => $v) {
            $array[] = $v['f_id'];
        }


        return view('rf/read',[
            'id'=>$id,
            'data'=>$data,
            'arr'=>$array,
            'uname'=>$uname['name']

        ]);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
