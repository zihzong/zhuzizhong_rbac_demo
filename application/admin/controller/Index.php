<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Session;



class Index extends Base
{
    /**
     * 后台登录页面
     * @return \think\response\View
     */
    public function index()
    {
        return view('admin@index/login');
    }

    /**
     * 后台登录处理
     * @return \think\response\View
     */
    public function logindo(Request $request)
    {

        //账号密码验证
        $p = $request->post();
        $name = $p['name'];
        $pass = md5($p['pass']);

        $res = Db::field(['pass','id'])
            ->table(['user'])
            ->where('name','=',$name)
            ->find();

        if ($res){
            if ($res['pass'] !== $pass){
                return $this->error('密码错误请重试!');
            } else {
                Session::set('user',$name);
                Session::set('id',$res['id']);


                return view('admin@main/index');
            }
        } else {
            return $this->error('账号不存在');
        }




    }

    /**
     * 后台登出处理
     * @return \think\response\View
     */
    public function logout()
    {

        Session::clear('');
//        Session::clear('fun');
//        Session::clear('admin');
//        Session::clear('role');
//        Session::clear('think');
//        Session::clear('index');
        return view('admin@index/login');
    }

}
