<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Session;

class User extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $list = Db::table('user')->field(['id','name'])->order(['id'=>'ASC'])->select();

        //通过id查所属角色
        foreach ($list as $k => $v) {
            $role = Db::field(['r.name'=>'rname','u.u_id'=>'uid'])
                ->table(['u_r'=>'u','role'=>'r'])
                ->where('u.r_id=r.id','u.u_id='.$v['id'])
                ->select();
        }

        return view('user/index',[
            'list' => $list,
            'role' => $role
        ]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        return view('user/create');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $p = $request->post();

        if ($p['pass'] !== $p['repass']){
            return $this->error('两次密码输入不一致');
        }

        $data = [
            'name' => $p['name'],
            'pass' => md5($p['pass'])

        ];

        $result = Db::table('user')->data($data)->insert();

        if ($result > 0) {
            return $this->success('安排上了~', url('admin/user/index'));
        } else {
            return $this->error('还未安排上,请换个姿势');
        }


    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {

    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $result = Db::table('user')->delete($id);

        if ($result > 0) {
            return $this->success('删除成功咯~', url('admin/user/index'));
        } else {
            return $this->error('删除失败咯~ 请重试', url('admin/user/index'));
        }
    }






}
