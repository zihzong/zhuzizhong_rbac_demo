<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Session;

class Fun extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
            $list = Db::table('fun')->field(['id','name','controller','function'])->order(['id'=>'ASC'])->select();

            return view('fun/index',[
                'list' => $list
            ]);

    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        return view('fun/create');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->post();

        if ($data['name'] == null){
            return $this->error('节点名不能为空');
        } else if ($data['controller'] == null){
            return $this->error('模块名不能为空');
        } else if ($data['function'] == null){
            return $this->error('操作名不能为空');
        }


        $result = Db::table('fun')->data($data)->insert();

        if ($result > 0) {
            return $this->success('安排上了~', url('admin/fun/index'));
        } else {
            return $this->error('还未安排上,请换个姿势');
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
