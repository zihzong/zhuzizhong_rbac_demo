<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

Route::get('/', 'index/Index/index');
Route::get('/admin', 'admin/index/index');
Route::post('/admin/index/logindo','admin/index/logindo');
Route::post('/admin/main/index','admin/main/index');
Route::get('/admin/index/logout','admin/index/logout');
Route::get('/admin/index/index','admin/index/index');


// 资源路由,7条路由
Route::resource('user', 'admin/user');
Route::resource('role', 'admin/role');
Route::resource('fun', 'admin/fun');
Route::resource('ur', 'admin/ur');
Route::resource('rf', 'admin/rf');


return [

];
